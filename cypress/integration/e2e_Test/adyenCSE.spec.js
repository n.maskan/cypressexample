var csePublicKey = "10001|A9BF73932CD9AEE1D13030A03C71FA56DA876475B020C2F08E4DFE8DE48B9AAFF6E156FA73C46B7B81F5430829B2B29A4019D49CDD127A2CE9C78CC775AA1BB49AFB4F48A307120D944B902CAF0F61C931245FD9A98989776D1EDAFBAFDC4ABD69793FD5ADD21D5CF93F717C7BC07567A8C446D2C94EDD18FD8DD259502BECF4EAD40A1B4C747A2574DC38A4A3DF3E2B64229A80963E366AE4A9B57A9CA40EA70730694B66DB6621E55114EE7EFC9B01C38472478C1D90EEC37509E82D5C16C7E665F35B3B6E47E4D0AA784AAC2F6574AD8F17945C248B8BCEFE1112BF99F68A65D6331E8AC6BFE6E878571649154E584F75422862BDDFB43AF432B54EEEE4E9"
var ccNumber = "5585558555855583"
var ccCvc = "737"
var ccHolderName = "MASTERCARD"
var ccExpiryMonth = "03"
var ccExpiryYear = "2030"

var page = require('webpage').create();
page.onConsoleMessage = function(msg) {
    console.log(msg);
};
page.open("about:blank",function(status) {	

  page.includeJs("http://qa-services/payment/js/adyen.encrypt.min.js",function() {	
	
	var result = page.evaluate(function(csePublicKey,ccNumber,ccCvc,ccHolderName,ccExpiryMonth,ccExpiryYear) {
				
		var key = csePublicKey;
		
		var cardData = {
        	number : ccNumber,
	        cvc : ccCvc,
	        holderName : ccHolderName,
	        expiryMonth : ccExpiryMonth,
	        expiryYear : ccExpiryYear,
	            generationtime :  new Date().toISOString()
	        };
		
		var cseInstance = adyen.encrypt.createEncryption(key, {});		
 
		return cseInstance.encrypt(cardData);
	
	},csePublicKey,ccNumber,ccCvc,ccHolderName,ccExpiryMonth,ccExpiryYear);
	console.log(result);

	phantom.exit();
	});
});