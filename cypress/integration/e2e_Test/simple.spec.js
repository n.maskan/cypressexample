const { should } = require("chai")

describe('Browser action', () => {

    it('should load Questico website ', () => {
        cy.visit('https://qa-www.questico.de/?platform=nf', {timeout: 200000})
        cy.url().should('include', 'qa-www.questico.de/?platform=nf')
    })

    it('should display correct number of expert default list', () =>{
        cy.get('ul li')
            .its('length')
            .should('be.gt',4)
    })


    it('should click on Registrieren Button', () => {
        cy.get('button').contains('Registrieren').click()
          .wait(10000)
        //cy.get('h2').contains('Jetzt registrieren und kostenloses Erstgespräch nutzen!')
        //cy.get('input').should('be.visible')
    })
    
    /*
    Cypress.Commands.add('iframe', (iframeSelector, elSelector) => {
        return cy
          .get(`iframe${iframeSelector || ''}`, { timeout: 10000 })
          .should($iframe => {
            expect($iframe.contents().find(elSelector||'body')).to.exist
          })
          .then($iframe => {
            return cy.wrap($iframe.contents().find('body'))
          })
      })*/

      it('check iframe content', () => {
          cy.iframe('[data-testid="iframe-auth"]').as('authIframe') //create alias
          cy.get('@authIframe') //use alias to efficiently chain commands
            .find('h1').contains('Neu bei Questico?')
        
    })


    


    /*it('should wait 3 seconds', () => {
        cy.wait(3000)
    })

    it('should pause the execution', () => {
        cy.pause()
    })

    it('should check for correct element on the page', () => {
        cy.get('button').should('be.visible')
    })*/

})
