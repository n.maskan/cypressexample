describe('Login / Logout Test',()=> {

    it('should load Questico website ', () => {
        cy.visit('https://qa-www.questico.de/?platform=nf', {timeout: 200000})
        cy.url().should('include', '?platform=nf')
    })

    it('should display correct number of expert default list', () =>{
        cy.get('ul li')
            .its('length')
            .should('be.gt',4)
    })


    it('should click on Anmelden Button', () => {
        cy.get('[data-testid="navbar-login"]').contains('Anmelden').click()
          .wait(10000)
        //cy.get('h2').contains('Jetzt registrieren und kostenloses Erstgespräch nutzen!')
        //cy.get('input').should('be.visible')
    })

    it('should fill invalid email address', () => {
        cy.get('iframe').iframe(() => {
            // Targets the input within the iframe element
            cy.get('#username').clear()
            cy.get('#username').type('fakeqemail.com')
          })
        
    })

    it('should fill invalid password', ()=> {
        cy.get('iframe').iframe(()=>{
            cy.get('#password').clear()
            cy.get('#password').type('atla')

        })
    })

    it('should submit Registrieren form', ()=> {
        cy.get('iframe').iframe(()=>{
            cy.get('form').submit()

        })
        
    })

    it('should display error message', ()=>{
        cy.get('iframe').iframe(()=>{
            cy.get('.Input_errorHint__X6GnZ').should('be.visible')

        })
        
    })
})