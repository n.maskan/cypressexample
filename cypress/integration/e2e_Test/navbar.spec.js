
describe('Navbar Test', () => {
    before(function(){
        cy.visit('https://qa-www.questico.de/?platform=nf', {timeout: 200000})
        cy.url().should('include', '?platform=nf')
    })

    it('should display Berater finden content', ()=>{
    
        cy.get('[data-testid="navbar-burger"]').trigger('mouseover')
        /*
        //cy.get('.div > div.MuiBox-root.jss10061 > header > div > div').trigger('mouseover')
        //cy.get('div').should('have.class', 'MuiBox-root jss62 jss12')
        //cy.xpath('//*[@id="__next"]/div/div[1]/header').trigger('mousemove',{ clientX: 547, clientY: 47 })
        cy.xpath('//*[@id="__next"]/div/div[1]/header').should('be.visible')
        cy.xpath('//*[@id="__next"]/div/div[1]/header').trigger('mousemove',{ clientX: 547, clientY: 47 })
        cy.xpath('//*[@id="__next"]/div/div[1]/header').invoke('mousemove')
        //let e = new Event('mousemove', { bubbles: true, cancelable: true })
        // give corresponding value to its target position
        //e.clientX=592;e.clientY=46
        // dispatch the event
        //selector.dispatchEvent(e)*/
        cy.contains('Berater finden').click()
        cy.wait(4000)
        cy.url().should('include', 'berater-finden')
        cy.get('h1').should('be.visible')
                    .should('have.text', 'Finde Deinen Berater')
                
    })

    it('should display Tarot & Kartenlegen content', () =>{
        cy.get('[data-testid="navbar-burger"]').trigger('mouseover')
        cy.contains('Tarot & Kartenlegen').click()
        cy.wait(4000)
        cy.url().should('include', 'tarot-kartenlegen')
        cy.get('h1').should('be.visible')
                    .should('have.text', 'Lass dir in die Karten schauen')
    })

    it('should display Hellsehen & Wahrsagen content', () =>{
        cy.get('[data-testid="navbar-burger"]').trigger('mouseover')
        cy.contains('Hellsehen & Wahrsagen').click()
        cy.wait(4000)
        cy.url().should('include', 'hellsehen-wahrsagen')
        cy.get('h1').should('be.visible')
                    .should('have.text', 'Brauchst du Gewissheit? ')
    })

    it('should display Medium & Channeling content', () =>{
        cy.get('[data-testid="navbar-burger"]').trigger('mouseover')
        cy.contains('Medium & Channeling').click()
        cy.wait(4000)
        cy.url().should('include', 'medium-channeling')
        cy.get('h1').should('be.visible')
                    .should('have.text', 'Dein Medium zur Geistigen Welt')
    })

    it('should display Astrologie & Horoskope content', () =>{
        cy.get('[data-testid="navbar-burger"]').trigger('mouseover')
        cy.contains('Astrologie & Horoskope').click()
        cy.wait(4000)
        cy.url().should('include', 'astrologie-horoskope')
        cy.get('h1').should('be.visible')
                    .should('have.text', 'Was hält dein Horoskop für dich bereit?')
    })

    it('should display Horoskop content', () =>{
        cy.get('[data-testid="navbar-burger"]').trigger('mouseover')
        cy.get('a[href*="/horoskop"]').click()
        //cy.contains('Horoskop').click()
        cy.wait(4000)
        cy.url().should('include', 'horoskop')
        cy.get('h1').should('be.visible')
                    .should('have.text', 'Horoskop')
    })

    it('should display Tageshoroskop content', () =>{
        cy.get('[data-testid="navbar-burger"]').trigger('mouseover')
        cy.contains('Tageshoroskop').click()
        cy.wait(4000)
        cy.url().should('include', 'tageshoroskop')
        cy.get('h1').should('be.visible')
                    .should('have.text', 'Dein kostenloses Tageshoroskop')
    })

    it('should display Monatshoroskop content', () =>{
        cy.get('[data-testid="navbar-burger"]').trigger('mouseover')
        cy.contains('Monatshoroskop').click()
        cy.wait(4000)
        cy.url().should('include', 'monatshoroskop')
        cy.get('h1').should('be.visible')
                    .should('have.text', 'Dein Monatshoroskop')
    })

    it('should display Magazine content', () =>{
        cy.get('[data-testid="navbar-burger"]').trigger('mouseover')
        cy.contains('Magazin').click()
        cy.wait(4000)
        cy.url().should('include', 'magazin')
        cy.get('h1').should('be.visible')
                    .should('have.text', 'Dein Questico Magazin')
    })

    it('should display Traumdeutung content', () =>{
        cy.get('[data-testid="navbar-burger"]').trigger('mouseover')
        cy.contains('Traumdeutung').click()
        cy.wait(4000)
        cy.url().should('include', 'traumdeutung')
        cy.get('h1').should('be.visible')
                    .should('have.text', 'Lexikon der Traumdeutung')
    })

    it('should display über uns content', () =>{
        cy.get('[data-testid="navbar-burger"]').trigger('mouseover')
        cy.contains('Über uns').click()
        cy.wait(4000)
        cy.url().should('include', 'wir-ueber-uns')
        cy.get('h1').should('be.visible')
                    .should('have.text', 'Wir über uns')
    })

    it('should display Hilfe content', () =>{
        cy.get('[data-testid="navbar-burger"]').trigger('mouseover')
        cy.contains('Hilfe').click()
        cy.wait(4000)
        cy.url().should('include', 'hilfe')
        cy.get('h1').should('be.visible')
                    .should('have.text', 'Hilfebereich')
    })
})