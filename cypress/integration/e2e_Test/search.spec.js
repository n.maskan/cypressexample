/// <reference types="Cypress" />

describe('Searchbox Test', ()=> {
    before(function() {
        cy.visit('https://qa-www.questico.de/?platform=nf', {timeout: 200000})
    })

    it('should type into searchbox and submit with pressing enter', () =>{
        cy.get('form').within(($form) =>{
            cy.get('.MuiInputBase-root').click()
            cy.get('input[name="search"]').type('test-expert-noure {enter}')
            
        })
            
    })

    it('should show search result page', () => {
        cy.url().should('include', 'search?search=test-expert-noure')
        cy.get('.jss402 > .MuiTypography-root').should('have.text',"Search Results for 'test-expert-noure'")
    })

    it('should type into searchbox and submit form', () =>{
        cy.get('form').within(($form) =>{
            cy.get('.MuiInputBase-root').click()
            cy.get('input[name="search"]').type('test-expert-noure')
            cy.root().submit()
        })
            
    })

    it('searchbox should be visible ', () =>{
        cy.get('.jss402 > .MuiTypography-root').should('be.visible')

    })

    it('should type into searchbox and select in the list', () => {
        cy.get('form').within(($form) =>{
            cy.get('.MuiInputBase-root').click()
            cy.get('input[name="search"]').type('test')           
        })
        
        //cy.findByRole('listbox').should('exist')
        cy.get('ul li:first').each((item, index) =>{
            cy.wrap(item)
            cy.should('contain.text', 'test expert noure')
            cy.contains('test expert noure').click()
        })
    
    })

    it('should show expert profile page', () => {
        cy.url().should('include', '/berater/test-expert-noure/2337573')
    })


})

